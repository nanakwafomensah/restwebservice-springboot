package com.example.restapispring.services;

import com.example.restapispring.entities.Roomcategory;
import org.springframework.stereotype.Service;

import java.util.List;


public interface RoomCategoryService {
    Roomcategory save(Roomcategory roomcategory);
    Roomcategory update(int id,Roomcategory roomcategory);
    void delete(int roomcategoryId);
    Roomcategory findRoomCategoryId(int roomcategoryId);
    List<Roomcategory> allroomCategory();
}
