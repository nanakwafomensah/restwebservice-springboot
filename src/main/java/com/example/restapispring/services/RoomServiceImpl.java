package com.example.restapispring.services;

import com.example.restapispring.entities.Room;
import com.example.restapispring.repositories.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class RoomServiceImpl implements RoomService {

    @Autowired
    private RoomRepository roomRepository;

    @Override
    public Room save(Room room) {
        roomRepository.save(room);
        return room;
    }

    @Override
    public Room update(int roomId, Room room) {
        Room roomSelected= roomRepository.getOne(roomId);
        roomSelected.setName(room.getName());
        roomSelected.setDescription(room.getDescription());
        roomSelected.setPhoto(room.getPhoto());
        roomSelected.setRoomcategory(room.getRoomcategory());
        roomRepository.save(roomSelected);
        return room;
    }

    @Override
    public void delete(int roomId) {
        roomRepository.deleteById(roomId);
    }

    @Override
    public Room getRoomById(int roomId) {
        return roomRepository.getOne(roomId);
    }

    @Override
    public List<Room> allRoom() {
        return roomRepository.findAll();
    }
}
