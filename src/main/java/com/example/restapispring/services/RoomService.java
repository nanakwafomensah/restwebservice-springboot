package com.example.restapispring.services;

import com.example.restapispring.entities.Room;
import org.springframework.stereotype.Service;

import java.util.List;


public interface RoomService {
    Room save(Room room);
    Room update(int roomId,Room room);
    void delete(int roomId);
    Room getRoomById(int roomId);

    List<Room> allRoom();
}
