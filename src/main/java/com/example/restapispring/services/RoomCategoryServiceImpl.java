package com.example.restapispring.services;

import com.example.restapispring.entities.Roomcategory;
import com.example.restapispring.repositories.RoomCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class RoomCategoryServiceImpl implements RoomCategoryService {

    @Autowired
    private RoomCategoryRepository roomCategoryRepository;

    @Override
    public Roomcategory save(Roomcategory roomcategory) {
        roomCategoryRepository.save(roomcategory);
        return roomcategory;
    }

    @Override
    public Roomcategory update(int id, Roomcategory roomcategory) {
        Roomcategory roomcategorySelected  =roomCategoryRepository.getOne(id);
        roomcategorySelected.setName(roomcategory.getName());
        roomcategorySelected.setDescription(roomcategory.getDescription());
        roomCategoryRepository.save(roomcategorySelected);
        return roomcategorySelected;
    }

    @Override
    public void delete(int roomcategoryId) {
         roomCategoryRepository.deleteById(roomcategoryId);
    }

    @Override
    public Roomcategory findRoomCategoryId(int roomcategoryId) {
        return roomCategoryRepository.getOne(roomcategoryId);
    }

    @Override
    public List<Roomcategory> allroomCategory() {
        return roomCategoryRepository.findAll();
    }
}
