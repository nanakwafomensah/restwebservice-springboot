package com.example.restapispring.resources;

import com.example.restapispring.entities.Room;
import com.example.restapispring.entities.Roomcategory;
import com.example.restapispring.services.RoomCategoryService;
import com.example.restapispring.services.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/resources/api/v1.0/roomcategory")
public class RoomCategoryResource {
    @Autowired
    private RoomCategoryService roomCategoryService;

    @RequestMapping(value = "/",method = RequestMethod.GET)
    public List<Roomcategory> allrooms(){
        return roomCategoryService.allroomCategory();
    }

    @RequestMapping(value = "/{roomcategoryId}",method = RequestMethod.GET)
    public Roomcategory getRoomById(@PathVariable("roomcategoryId") int id){
        return  roomCategoryService.findRoomCategoryId(id);
    }
    //we want to post data to database in json format
    @RequestMapping(value="/create",method = RequestMethod.POST,consumes="application/json")
    public ResponseEntity<Roomcategory> addRoomCategory(@RequestBody Roomcategory roomcategory){
          roomCategoryService.save(roomcategory);
          return new ResponseEntity<>(roomcategory, HttpStatus.OK);
    }
    //we want to post data to database from a form
    @RequestMapping(value="/create",method = RequestMethod.POST,consumes= MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public ResponseEntity<Roomcategory> newroomcategory(@RequestParam("name") String name,
                                                        @RequestParam("description")String description){
        Roomcategory newroomcategory =new Roomcategory(name,description);
        roomCategoryService.save(newroomcategory);
        return new ResponseEntity<>(newroomcategory, HttpStatus.OK);
    }
    //Update
    @RequestMapping(value="/update/{roomcategory_id}",method = RequestMethod.PUT,consumes="application/json")
    public ResponseEntity<Roomcategory> updateRoomCategory(@PathVariable("roomcategory_id") int id,@RequestBody Roomcategory roomcategory){
        roomCategoryService.update(id,new Roomcategory(roomcategory.getName(),roomcategory.getDescription()));
        return new ResponseEntity<>(roomcategory, HttpStatus.OK);
    }
    //delete
    @RequestMapping(value="/delete/{roomcategory_id}",method = RequestMethod.DELETE)
    public ResponseEntity<Roomcategory> deleteRoomCategory(@PathVariable("roomcategory_id") int id){
        roomCategoryService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }


//    In certain situations (for example, when the service or its consumers are behind an
//            overzealous corporate frewall, or if the main consumer is a web page), only the GET
//    and POST HTTP methods might be available
     @RequestMapping(value = "update/{roomcategory_id}", method = RequestMethod.POST, headers = {"X-HTTP-Method-Override=PUT"})
     public ResponseEntity<Roomcategory> updateRoomAsPost(@PathVariable("roomcategory_id") int id,@RequestBody Roomcategory roomcategory){
         roomCategoryService.update(id,new Roomcategory(roomcategory.getName(),roomcategory.getDescription()));
         return new ResponseEntity<>(roomcategory, HttpStatus.OK);
     }
}
