package com.example.restapispring.resources;


import com.example.restapispring.entities.Room;
import com.example.restapispring.services.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/resources/api/v1.0/rooms")
public class RoomResource {

  @Autowired
  private RoomService roomService;

  @RequestMapping(value = "/",method = RequestMethod.GET)
  public List<Room> allrooms(){
      return roomService.allRoom();
  }

  @RequestMapping(value = "/{roomId}",method = RequestMethod.GET)
  public Room getRoomById(@PathVariable("roomId") int id){
      return  roomService.getRoomById(id);
  }
}
