package com.example.restapispring.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="rooms")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Room  {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "name")
    private String name;
    @Column(name="description")
    private String description;
    @Column(name="photo")
    private String photo;
    @ManyToOne
    @JoinColumn(name = "roomcategory_id",nullable = false)
    private Roomcategory roomcategory;

    public Room(){}
    public Room(String name, String description, String photo, Roomcategory roomcategory) {
        this.name = name;
        this.description = description;
        this.photo = photo;
        this.roomcategory = roomcategory;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Roomcategory getRoomcategory() {
        return roomcategory;
    }

    public void setRoomcategory(Roomcategory roomcategory) {
        this.roomcategory = roomcategory;
    }

    @Override
    public String toString() {
        return "Room{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", photo='" + photo + '\'' +
                ", roomcategory=" + roomcategory +
                '}';
    }
}
