package com.example.restapispring.repositories;

import com.example.restapispring.entities.Roomcategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


public interface RoomCategoryRepository extends JpaRepository<Roomcategory,Integer> {
}
